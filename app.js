const squaresRef = document.querySelectorAll(".square");
const squaresArr = [...squaresRef];
const result = document.querySelector(".result");
const hint = document.querySelector(".hint");
const header = document.querySelector(".header");
const newGameBtn = document.querySelector(".btn");
const modeButtons = document.querySelectorAll(".mode");
let colorNums = 6;
let colors = generateRandomColors(6);
let winningColor = pickColor();
result.innerHTML = winningColor;

squaresArr.forEach(
  (square, index) => (square.style.backgroundColor = colors[index])
);

squaresArr.forEach(square => square.addEventListener("click", colorValidation));

newGameBtn.addEventListener("click", reset);

modeButtons.forEach(button =>
  button.addEventListener("click", function() {
    modeButtons[0].classList.remove("selected");
    modeButtons[1].classList.remove("selected");
    this.classList.add("selected");
    if (this.textContent === "Hard") {
      squaresArr.forEach(square => square.classList.remove("hide"));
    }
    this.textContent === "Easy" ? (colorNums = 3) : (colorNums = 6);
    reset();
  })
);

function reset() {
  colors = generateRandomColors(colorNums);
  winningColor = pickColor();
  result.innerHTML = winningColor;
  squaresArr.forEach((square, index) => {
    if (colors[index]) {
      square.style.backgroundColor = colors[index];
    } else {
      squaresArr[index].classList.add("hide");
    }
  });
  header.style.backgroundColor = "steelblue";
  newGameBtn.textContent = "New Colors";
  hint.textContent = "";
}

function colorValidation() {
  let clickedColor = this.style.backgroundColor;
  if (clickedColor === winningColor) {
    hint.textContent = "Correct!";
    squaresArr.forEach(square => (square.style.backgroundColor = winningColor));
    header.style.backgroundColor = winningColor;
    newGameBtn.textContent = "Play again?";
  } else {
    this.style.backgroundColor = "#232323";
    hint.textContent = "Try again!";
  }
}

function pickColor() {
  let random = Math.floor(Math.random() * colors.length);
  return colors[random];
}

function generateRandomColors(num) {
  let colorArr = [];
  for (let i = 0; i < num; i++) {
    let red = Math.floor(Math.random() * 255 + 1);
    let blue = Math.floor(Math.random() * 255 + 1);
    let green = Math.floor(Math.random() * 255 + 1);

    let randomColor = `rgb(${red}, ${green}, ${blue})`;

    colorArr.push(randomColor);
  }
  return colorArr;
}
